Categories:System
License:GPLv3
Web Site:https://github.com/javiersantos/MLManager/blob/HEAD/README.md
Source Code:https://github.com/javiersantos/MLManager
Issue Tracker:https://github.com/javiersantos/MLManager/issues
Changelog:https://github.com/javiersantos/MLManager/blob/HEAD/CHANGELOG.md

Name:ML Manager (root)
Auto Name:ML Manager
Summary:Manage apps
Description:
Modern, easy and customizable app manager with Material Design. This is the
"pro" version of [[com.javiersantos.mlmanagerpro]] for rooted devices.
.

Requires Root:yes

Repo Type:git
Repo:https://github.com/javiersantos/MLManager

Build:1.0.3,19
    commit=v1.0.3
    subdir=app
    gradle=pro

Build:1.0.4,20
    commit=v1.0.4
    subdir=app
    gradle=pro

Archive Policy:0 versions
Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.0.4
Current Version Code:20

