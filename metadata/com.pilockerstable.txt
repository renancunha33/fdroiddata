Categories:Theming
License:GPLv2
Web Site:http://forum.xda-developers.com/showthread.php?p=53218143
Source Code:https://github.com/Pi-Developers/Pi-Locker
Issue Tracker:https://github.com/Pi-Developers/Pi-Locker/issues

Auto Name:Pi Locker
Summary:Lockscreen
Description:
A smart and clean lockscreen replacement with unlocking gestures. It features
lightweight design for usage even with low-RAM devices, PIN and password
security, changeable colors and texts and the ability to hide/unhide the status
bar.
.

Repo Type:git
Repo:https://github.com/Pi-Developers/Pi-Locker

Build:3.0,3
    commit=17ef7d58cee1fe9776a790371fe9ebfa93c6d7ce
    extlibs=android/android-support-v4.jar

Build:5.0.3,10
    disable=todo
    commit=4622bb9b10074859f333af0b2659f4f4e06d12c7
    extlibs=android/android-support-v4.jar

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:5.0.3
Current Version Code:10

