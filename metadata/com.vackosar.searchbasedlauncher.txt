Categories:Theming
License:MIT
Web Site:
Source Code:https://github.com/vackosar/search-based-launcher/
Issue Tracker:https://github.com/vackosar/search-based-launcher/issues
Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=vackosar%40gmail%2ecom&lc=US&item_name=ideasfrombrain&item_number=Search%20based%20launcher&no_note=0&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHostedGuest

Auto Name:Search based launcher v3
Summary:Minimalistic home-screen
Description:
* When the results narrow down to one app, it can be automaticaly started (Autostart button).
* Add any activity of any app on your device to list of apps (e.g. Wifi settings, ...).
* Hide any app from list of apps adding it to the hide list.
* Rename any app however you like.
* Icons of apps and wallpapers are not displayed, freeing up RAM.
* The space character " " is mapped to the "any set of characters". (i.e. in terms of REGEX " " is replaced with "*." )
* Advanced search using REGEX.
* Includes basic widgets for Wifi, camera etc.
* Works as a home-screen or normal app
.

Repo Type:git
Repo:https://github.com/ideasfrombrain/search-based-launcher-v2.git

Build:39,39
    commit=d09b2174a5afce7fdfce1941b09e47718ad2c168
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:39
Current Version Code:39

